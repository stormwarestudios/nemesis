package initializer

type Initializer interface {
	Initialize() error
	Start() error
	Stop() error
}

