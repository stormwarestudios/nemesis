package logger

import (
	"github.com/Sirupsen/logrus"
	"os"
)

type Logger struct {
	loadPriority  int32
	startPriority int32
	stopPriority  int32
}

type LogLevel int32

const (
	Trace     LogLevel = 0
	Debug     LogLevel = 1
	Info      LogLevel = 2
	Warning   LogLevel = 3
	Error     LogLevel = 4
	Fatal     LogLevel = 5
	Panic     LogLevel = 6
)

func (l Logger) Initialize() {
	if os.Getenv("ENVIRONMENT") == "production" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{})
	}

	logrus.Infof("Logger.Initialize()")
}

func (l Logger) Start() {

}

func (l Logger) Stop() {

}

// Log outputs the given text to the log system
func (l Logger) Log(text string, level LogLevel) {
	switch (level) {
	case Trace:
		logrus.Tracef(text)

	case Debug:
		logrus.Debugf(text)

	case Info:
		logrus.Infof(text)

	case Warning:
		logrus.Warningf(text)

	case Error:
		logrus.Errorf(text)

	case Fatal:
		logrus.Fatalf(text)

	case Panic:
		logrus.Panicf(text)
	}
}

func NewLogger() (Logger, error) {
	logger := Logger{}
	logger.loadPriority = 1000
	logger.startPriority = 1000
	logger.stopPriority = 1000

	return logger, nil
}
