package nemesis

import (
	"os"

	"gitlab.com/stormwarestudios/nemesis/logger"
)

// Initialize sets up the Nemesis platform.
func Initialize() {
	log, err := logger.NewLogger()
	if err != nil {
		panic(err)
		os.Exit(1)
	}

	log.Log("Nemesis started up", logger.Info)

	log.Log("Trace", logger.Trace)
	log.Log("Debug", logger.Debug)
	log.Log("Info", logger.Info)
	log.Log("Warning", logger.Warning)
	log.Log("Error", logger.Error)
	log.Log("Fatal", logger.Fatal)
	log.Log("Panic", logger.Panic)
}
